require('dotenv').config({path:__dirname + '/.env'});
const express = require('express')
const mysql = require('mysql')
const connection = mysql.createConnection({
  host: `${process.env.DB_HOST}`,
  user: `${process.env.DB_USER}`,
  password: `${process.env.DB_PASSWORD}`,
  database: `${process.env.DB_NAME}`
})
connection.connect();

//Status de los tiles.
const STATUS_NOT_PLAYED = 0;
const STATUS_SOLVED = 1;
const STATUS_INVALID = 2;

let latam_countries = []

const app = express()
const port = process.env.PORT || 3000;

app.get('/', handleRequest)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

 async function createTiles(rows) {
  let tiles = await Promise.all(
    rows.map(async(item) => {
      let entries = await createGreenOptions(item)
      entries.push({ "type": "white", "decision": "skip", "label": "Siguiente" });
      entries.push({ "type": "blue", "decision": "no", "label": "Ninguna opción es la correcta" });
      return {
        id: item.qid,
        sections: [ {
          "type": "text",
          "title": "Selecciona el país de nacionalidad de esta persona"
        },
        {
          "type": "item",
          "q": item.qid
        }],
        controls: [{
            "type": "buttons",
            "entries": entries
        }]
      };
    })
  );
  return {
    "tiles" : tiles
  }
}

//crea las opciones válidas para cada tile
async function createGreenOptions(item){
  //puedes enviar las mismas opciones verdes a todos los tiles
  //puedes tener las opciones guardadas en otra tabla de tu bbdd
  //o algo más sofisticado

  //en este ejemplo, cada tile pertenece a un país, y las opciones verdes son ciudades de ese país
  //consultamos una tabla de ciudades para construir las opciones verdes
  let countries = await getCountries(item.qid)
  let options = countries.map(function (country, index) {
    value_qid =country.qid.replace("Q","")
    return {
      "type": "green",
      "decision": "yes",
      "label": country.nombre, //etiqueta que aparecerá en el botón
      "api_action": {
          "action": "wbcreateclaim",
          "entity": item.qid,
          "property": "P27", //la propiedad que vas a modificar
          "snaktype": "value",
          "value": `{\"entity-type\":\"item\",\"numeric-id\":${value_qid}}` //el valor que le darás a esa propiedad
      }
    }
  });
  return options
}

async function getCountries(qid) {
  maxOptions = 10

  //obtener países que aparecían en el artículo de wikipedia  
  sqlQuery = `SELECT p.qid, p.nombre
    FROM paises p
    INNER JOIN pais_tile pt ON p.codigo = pt.iso_code
    WHERE pt.tile_qid = '${qid}'
    LIMIT ${maxOptions};`

  let mentionedCountries = await queryDatabase(sqlQuery)
  let latam_countries = await getLatamCountries()

  return mentionedCountries.length ? mentionedCountries :  latam_countries
}

async function getLatamCountries() {
  if (!latam_countries.length){
    let sqlQuery = `SELECT qid, nombre
      FROM paises p
      WHERE latam = 1
      ORDER BY ranking_poblacion ASC
      LIMIT 10;`

    latam_countries = await queryDatabase(sqlQuery)
  }
  return latam_countries
}

function queryDatabase(sqlQuery) {
  return new Promise((resolve, reject) => {
    connection.query(sqlQuery, (err, rows, fields) => {
      if (err) {
        console.log("QUERY FAILED")
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
}

//registramos que un usuario marca una tile como inválida
function logNo(id, user) {
  console.log("log no")
  sqlQuery = `UPDATE tiles
  SET status = ${STATUS_INVALID}, usuario = '${user}'
  WHERE qid = '${id}';`;
  console.log(sqlQuery)
  queryDatabase(sqlQuery);
}

//registramos que un usuario marca una opción correcta
function logYes(id, user) {
  console.log("log yes")
  sqlQuery = `UPDATE tiles
  SET status = ${STATUS_SOLVED}, usuario = '${user}'
  WHERE qid = '${id}';`;
  console.log(sqlQuery)
  queryDatabase(sqlQuery);
}

async function handleRequest(req, res) {
  try {
    const action = req.query.action;
    const callback = req.query.callback;
    switch (action) {
      case "desc":
        //título, descripción e ícono
        let descJson = {
          "label" : {
            "en" : "Name of The Game in English",
            "es" : "Nombre del Juego en Español",
            //agrega más idiomas
          },
          "description" : {
            "en" : "Game description in English. Give the players clear instructions and some context.",
            "es" : "Descripción del juego en español. Dale a les jugadores instrucciones claras y algo de contexto.",
            //agrega más idiomas
          },
          //reemplaza por otro icono bonito e ilustrativo
          "icon" : 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/WU_logo_3.png/640px-WU_logo_3.png'
        }
        res.send(`${callback}(${JSON.stringify(descJson)})`);
        break;
      
      case "tiles":
        console.log("requested tiles")
        //si la query no especifica cuantas tiles, enviaremos 5
        const num = req.query?.num ? req.query.num : 5;
        
        //traemos solo las tiles que no han sido jugadas
        //un orden random mitiga la posibilidad de que se jueguen dos tiles a la vez
        sqlQuery = `SELECT * 
        FROM tiles
        WHERE status = ${STATUS_NOT_PLAYED}
        ORDER BY RAND()
        LIMIT ${num};`

        //traemos los items
        let items = await queryDatabase(sqlQuery)
        //creamos los tiles
        let tiles = await createTiles(items);
        //enviamos los tiles
        res.send(`${callback}(${JSON.stringify(tiles)})`);
        break;

      case "log_action":
        let decision = req.query.decision
        let user = req.query.user
        let tile = req.query.tile
        
        if (decision=="yes") {
          logYes(tile,user)
        } else if (decision=="no"){
          logNo(tile,user)
        }
        break;
    
      default:
        res.json({
          error: 'Parámetro "action" inválido'
        });
    }
  } catch (error) {
    console.error('Uncaught Exception:', error);
  }
}

process.on('uncaughtException', (error) => {
  console.error('Uncaught Exception:', error);
  // Realizar acciones de limpieza si es necesario
  process.exit(1); // Forzar la salida de la aplicación con un código de error
});
