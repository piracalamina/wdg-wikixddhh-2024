# wdg-boilerplate

Esto es un boilerplate para crear APIs de Juegos para The Distributed Game con node.js

## .env

- Crea un archivo .env en la raíz de este proyecto y completá
```
DB_HOST=host
DB_USER=user
DB_PASSWORD=password
DB_NAME=name
```

Si estás usando toolforge, consultá https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database para obtenerlos.
